# matrix compose deployment

This is a script to easily deploy a matrix server locally using docker-compose

## Requirements
1. Docker
2. docker-compose
3. wget


## Instructions

Run the configure script to create the structure the compose file expects by passing the host name of your machine.
eg.

```shell
$ ./configure.sh pkarakal.com
```

and then just do a `docker-compose --profile main up -d`.


To also spin up a keycloak server change the composed profile to all `docker-compose --profile all up -d`