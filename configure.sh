#!/usr/bin/env bash

if [ -d data ];\
then
  rm -rf data synapse.$1
  echo "Deleted old data"
fi

HOST="$1"

mkdir -p data/postgres
mkdir -p data/traefik
mkdir -p data/matrix/{nginx,synapse,riot}
mkdir -p synapse.universis-test01.it.auth.gr
mkdir -p data/matrix/synapse/media_store

if [ -f homeserver.yaml ]; \
then
  cp homeserver.yaml data/matrix/synapse/homeserver.yaml
fi

sed -i -r -e "s|example\.com|$HOST|g" data/matrix/synapse/homeserver.yaml

if [ -f config.json ]; \
then
  cp config.json data/matrix/riot/config.json
fi

sed -i -r -e "s|http\:\/\/synapse\.localhost|https\://synapse\.$HOST|g" data/matrix/riot/config.json
sed -i -r -e "s|localhost|$HOST|g" data/matrix/riot/config.json
cp docker-compose.dev.yml docker-compose.yml
sed -i -r -e "s|example\.com|$HOST|g" docker-compose.yml

cp my.matrix.host.* ./data/matrix/synapse/

wget traefik.me/fullchain.pem -O synapse.$HOST/fullchain1.pem
wget traefik.me/cert.pem -O synapse.$HOST/cert.pem
wget traefik.me/privkey.pem -O synapse.$HOST/privkey1.pem
